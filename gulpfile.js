(function() {
    "use strict";

    var gulp = require('gulp'),
        cssshrink = require('gulp-cssshrink'),
        concat = require('gulp-concat'),
        coffee = require('gulp-coffee'),
        spritesmith = require('gulp.spritesmith'),
        stylus = require('gulp-stylus'),
        uglify = require('gulp-uglify');

    // css -> compress
    gulp.task('css', function(){
        gulp.src("_/stylus/*.styl")
            .pipe(stylus())
            .pipe(cssshrink())
            .pipe(gulp.dest('css/'));
    });

    gulp.task('concat', function() {
        return gulp.src("_/coffee/modules/*.coffee")
            .pipe(concat("main.coffee"))
            .pipe(gulp.dest("_/coffee/"));
        // gulp.run('js');
    });

    // concat -> coffee -> uglify -> js
    gulp.task('js', function(){
        gulp.src("_/coffee/*.coffee")
            .pipe(coffee())
            .pipe(uglify())
            .pipe(gulp.dest("js/"));
    });

    // sprite/*.png -> sprite.png + sprite.styl
    gulp.task('sprite', function () {
        var spriteData = gulp.src('img/sprite/*.png').pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.styl',
            imgPath: '../img/sprite.png',
            padding: 2,
            cssFormat: 'stylus'
        }));

        spriteData.img.pipe(gulp.dest('img/'));
        spriteData.css.pipe(gulp.dest('_/stylus/base/'));
    });

    // watchers
    gulp.task('watch', function() {
        gulp.watch(["_/stylus/**/*.styl", "_/stylus/*.styl"], ['css']);
        gulp.watch('_/coffee/modules/*.coffee', ['concat']);
        gulp.watch('_/coffee/*.coffee', ['js']);
        gulp.watch('img/sprite/*.png', ['sprite']);
    });

    gulp.task('default', ['css', 'js', 'concat', 'watch']);

})();