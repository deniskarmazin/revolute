_win = window
_doc = document
_html = _doc.documentElement
_body = _doc.body

_win.Currency = {}
data = {
    labels: ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"],

    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        },
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        },
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        },
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        }
    ]
}

data2 = {
    labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],

    datasets: [
        {
            label: "My Second dataset",
            fillColor: "#c0392b",
            strokeColor: "#c0392b",
            pointHighlightFill: "#c0392b",
            data: [22.7, 21.6, 22.8, 22.5, 21.3, 22.6, 21.7, 20.7, 21.3, 21.7, 22, 21.3, 22.6, 21.7, 22.4, 22.4, 21.3, 22, 21.8, 22.7, 21.9, 22.6, 22, 22, 21.6, 21, 22.9, 22.7, 22.8, 22.9]
        },
        {
            label: "My First dataset",
            fillColor: "#3498db",
            strokeColor: "#3498db",
            pointHighlightFill: "#3498db",
            data: [22.5, 21.3, 22.7, 22, 21.3, 22.4, 21.7, 20.5, 21.3, 21.7, 22, 21.3, 22.4, 21.7, 22.4, 22.1, 21.3, 22, 21.6, 22.5, 21.4, 22.5, 22, 22, 21.6, 21, 22.5, 22, 22.6, 22.6]
        }
    ]
}

options = {
# Boolean - Whether to animate the chart
    animation: true,

# Number - Number of animation steps
    animationSteps: 60,

# String - Animation easing effect
# Possible effects are:
# [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
#  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
#  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
#  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
#  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
#  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
#  easeOutElastic, easeInCubic]
    animationEasing: "easeOutQuart",

# Boolean - If we should show the scale at all
    showScale: true,

    pointDot: false,

# Boolean - If we want to override with a hard coded scale
    scaleOverride: false,

# ** Required if scaleOverride is true **
# Number - The number of steps in a hard coded scale
    scaleSteps: null,
# Number - The value jump in the hard coded scale
    scaleStepWidth: null,
# Number - The scale starting value
    scaleStartValue: null,

# String - Colour of the scale line
    scaleLineColor: "rgba(0,0,0,.1)",

# Number - Pixel width of the scale line
    scaleLineWidth: 1,

# Boolean - Whether to show labels on the scale
    scaleShowLabels: true,

# Interpolated JS string - can access value
    scaleLabel: "<%=value%>",

# Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
    scaleIntegersOnly: false,

# Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: false,

# String - Scale label font declaration for the scale label
    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

# Number - Scale label font size in pixels
    scaleFontSize: 12,

# String - Scale label font weight style
    scaleFontStyle: "normal",

# String - Scale label font colour
    scaleFontColor: "#666",

# Boolean - whether or not the chart should be responsive and resize when the browser does.
    responsive: true,

# Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,

# Boolean - Determines whether to draw tooltips on the canvas or not
    showTooltips: true,

# Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
    customTooltips: false,

# Array - Array of string names to attach tooltip events
    tooltipEvents: ["mousemove", "touchstart", "touchmove"],

# String - Tooltip background colour
    tooltipFillColor: "rgba(0,0,0,0.8)",

# String - Tooltip label font declaration for the scale label
    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

# Number - Tooltip label font size in pixels
    tooltipFontSize: 14,

# String - Tooltip font weight style
    tooltipFontStyle: "normal",

# String - Tooltip label font colour
    tooltipFontColor: "#fff",

# String - Tooltip title font declaration for the scale label
    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

# Number - Tooltip title font size in pixels
    tooltipTitleFontSize: 14,

# String - Tooltip title font weight style
    tooltipTitleFontStyle: "bold",

# String - Tooltip title font colour
    tooltipTitleFontColor: "#fff",

# Number - pixel width of padding around tooltip text
    tooltipYPadding: 6,

# Number - pixel width of padding around tooltip text
    tooltipXPadding: 6,

# Number - Size of the caret on the tooltip
    tooltipCaretSize: 8,

# Number - Pixel radius of the tooltip border
    tooltipCornerRadius: 6,

# Number - Pixel offset from point x to tooltip edge
    tooltipXOffset: 10,

# String - Template string for single tooltips
    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

# String - Template string for multiple tooltips
    multiTooltipTemplate: "<%= value %>",

# Function - Will fire on animation progression.
#    onAnimationProgress: function(){},

# Function - Will fire on animation completion.
#    onAnimationComplete: function(){}
}
Currency.DailyChart = do ->
    CV =
        canvas: _doc.querySelector '#daily_chart'
        currentRate: $ '.current'
        maxDay: $ '.max'
        dayName: $ '#dayName'
        selectedDayName: $ '#selectedDayName'
        dayInfo: $ '#dayInfo'
        selectedDayInfo: $ '#selectedDayInfo'
        calendar: $ '#datepicker'
        calendarLink: $ 'td[data-event="click"]'

    #initialize canvas with daily charts
    initCanvas = ->
        ctx = CV.canvas.getContext "2d"
        chart = new Chart(ctx).Bar(data, options)

    #initialize calendar
    initCalendar = ->
        CV.calendar.datepicker({
            maxDate: new Date(),
            onSelect: ->
                do showSelectedInfo
                do setInfo
                do scrollToInfo
            })

    getCurrentDate = ->
        dayName = CV.calendar.datepicker("getDate")
        CV.dayName.text($.datepicker.formatDate('DD', dayName))
        CV.dayInfo.text($.datepicker.formatDate('dd.mm.yy', dayName))

    setInfo =  ->
        dayName = CV.calendar.datepicker("getDate")
        CV.selectedDayName.text($.datepicker.formatDate('DD', dayName))
        CV.selectedDayInfo.text($.datepicker.formatDate('dd.mm.yy', dayName))

    showSelectedInfo = ->
        $('.c__daily_selected_h').hide()
        $('.c__daily_selected .c__daily_header').show()

    scrollToInfo = ->
        h = $('.c__daily_wrap').height()
        $('html, body').animate({scrollTop: h}, 500)

    #finding the current rate of currency
    current = ->
        currentRate = (data.datasets[0].data[data.datasets[0].data.length - 1]).toFixed(2)
        CV.currentRate.text(currentRate)

    #calculating the maximum rate of currency of the day
    max = ->
        maxArr = []
        $.each(data.datasets, ->
            maxFromPeriod = Math.max.apply(Math, @.data).toFixed(2)
            maxArr.push(maxFromPeriod)
        )
        maxDay = Math.max.apply(Math, maxArr).toFixed(2)
        CV.maxDay.text(maxDay)

    getData = (timeout) ->
        ajax_call = ->
            $.ajax({
                url: "http://104.236.42.50/revolut/index.php",
                timeout: 3000
            }).success( (data) ->
                console.log(data)
            ).fail((jqXHR, textStatus) ->
                if(textStatus is 'timeout')
                    alert('Failed from timeout')
            )

        interval = 1000 * 60 * 15
        ajax_call()
        setInterval(ajax_call, interval);

    init = ->
        do initCanvas
        do initCalendar
        do current
        do max
        do getCurrentDate
        do getData

    return {
        init: init
    }
Currency.MonthlyChart = do ->
    CV =
        canvas: _doc.querySelector '#monthly_chart'
        averageMonth: $ '.c__monthly_info_average'
        maxMonth: $ '.c__monthly_info_max'
        monthpicker: $ '#monthpicker'
        selectedMonthName: $ '#selectedMonthName'
        selectedYear: $ '#selectedYear'

    #initialize canvas with daily charts
    initCanvas = ->
        ctx = CV.canvas.getContext "2d"
        chart = new Chart(ctx).Line(data2, options)

    initCalendar = ->
        CV.monthpicker.datepicker({
            maxDate: new Date(),
            changeMonth: false,
            nextText: "Next",
            prevText: "Previous",
            onChangeMonthYear: (year, month) ->
                do showSelectedInfo
                CV.selectedYear.text(year)
                monthArr = []
                monthArr[1] = "January"
                monthArr[2] = "February"
                monthArr[3] = "March"
                monthArr[4] = "April"
                monthArr[5] = "May"
                monthArr[6] = "June"
                monthArr[7] = "July"
                monthArr[8] = "August"
                monthArr[9] = "September"
                monthArr[10] = "October"
                monthArr[11] = "November"
                monthArr[12] = "December"
                CV.selectedMonthName.text(monthArr[month])
                do scrollToInfo
        })

    setInfo =  ->
        monthName = CV.monthpicker.datepicker("getDate")


    showSelectedInfo = ->
        $('.c__monthly_selected_h').hide()
        $('.c__monthly_selected .c__monthly_header').show()

    scrollToInfo = ->
        h = $('.c__daily').height()
        $('html, body').animate({scrollTop: h}, 500)

    #calculating the average rate of currency of the day
    average = ->
        sum = 0
        $.each(data.datasets[0].data, ->
            sum+=parseFloat(this) || 0
        );
        averageMonth = (sum / data.datasets[0].data.length).toFixed(2)
        CV.averageMonth.text(averageMonth)

    #calculating the maximum rate of currency of the day
    max = ->
        maxMonth = Math.max.apply(Math, data.datasets[0].data)
        CV.maxMonth.text(maxMonth)

    init = ->
        do initCanvas
        do average
        do max
        do initCalendar

    return {
    init: init
    }
(($) ->
    do Currency.DailyChart.init
    do Currency.MonthlyChart.init

    return
) jQuery