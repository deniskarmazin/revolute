Currency.MonthlyChart = do ->
    CV =
        canvas: _doc.querySelector '#monthly_chart'
        averageMonth: $ '.c__monthly_info_average'
        maxMonth: $ '.c__monthly_info_max'
        monthpicker: $ '#monthpicker'
        selectedMonthName: $ '#selectedMonthName'
        selectedYear: $ '#selectedYear'

    #initialize canvas with daily charts
    initCanvas = ->
        ctx = CV.canvas.getContext "2d"
        chart = new Chart(ctx).Line(data2, options)

    initCalendar = ->
        CV.monthpicker.datepicker({
            maxDate: new Date(),
            changeMonth: false,
            nextText: "Next",
            prevText: "Previous",
            onChangeMonthYear: (year, month) ->
                do showSelectedInfo
                CV.selectedYear.text(year)
                monthArr = []
                monthArr[1] = "January"
                monthArr[2] = "February"
                monthArr[3] = "March"
                monthArr[4] = "April"
                monthArr[5] = "May"
                monthArr[6] = "June"
                monthArr[7] = "July"
                monthArr[8] = "August"
                monthArr[9] = "September"
                monthArr[10] = "October"
                monthArr[11] = "November"
                monthArr[12] = "December"
                CV.selectedMonthName.text(monthArr[month])
                do scrollToInfo
        })

    setInfo =  ->
        monthName = CV.monthpicker.datepicker("getDate")


    showSelectedInfo = ->
        $('.c__monthly_selected_h').hide()
        $('.c__monthly_selected .c__monthly_header').show()

    scrollToInfo = ->
        h = $('.c__daily').height()
        $('html, body').animate({scrollTop: h}, 500)

    #calculating the average rate of currency of the day
    average = ->
        sum = 0
        $.each(data.datasets[0].data, ->
            sum+=parseFloat(this) || 0
        );
        averageMonth = (sum / data.datasets[0].data.length).toFixed(2)
        CV.averageMonth.text(averageMonth)

    #calculating the maximum rate of currency of the day
    max = ->
        maxMonth = Math.max.apply(Math, data.datasets[0].data)
        CV.maxMonth.text(maxMonth)

    init = ->
        do initCanvas
        do average
        do max
        do initCalendar

    return {
    init: init
    }