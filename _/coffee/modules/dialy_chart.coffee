Currency.DailyChart = do ->
    CV =
        canvas: _doc.querySelector '#daily_chart'
        currentRate: $ '.current'
        maxDay: $ '.max'
        dayName: $ '#dayName'
        selectedDayName: $ '#selectedDayName'
        dayInfo: $ '#dayInfo'
        selectedDayInfo: $ '#selectedDayInfo'
        calendar: $ '#datepicker'
        calendarLink: $ 'td[data-event="click"]'

    #initialize canvas with daily charts
    initCanvas = ->
        ctx = CV.canvas.getContext "2d"
        chart = new Chart(ctx).Bar(data, options)

    #initialize calendar
    initCalendar = ->
        CV.calendar.datepicker({
            maxDate: new Date(),
            onSelect: ->
                do showSelectedInfo
                do setInfo
                do scrollToInfo
            })

    getCurrentDate = ->
        dayName = CV.calendar.datepicker("getDate")
        CV.dayName.text($.datepicker.formatDate('DD', dayName))
        CV.dayInfo.text($.datepicker.formatDate('dd.mm.yy', dayName))

    setInfo =  ->
        dayName = CV.calendar.datepicker("getDate")
        CV.selectedDayName.text($.datepicker.formatDate('DD', dayName))
        CV.selectedDayInfo.text($.datepicker.formatDate('dd.mm.yy', dayName))

    showSelectedInfo = ->
        $('.c__daily_selected_h').hide()
        $('.c__daily_selected .c__daily_header').show()

    scrollToInfo = ->
        h = $('.c__daily_wrap').height()
        $('html, body').animate({scrollTop: h}, 500)

    #finding the current rate of currency
    current = ->
        currentRate = (data.datasets[0].data[data.datasets[0].data.length - 1]).toFixed(2)
        CV.currentRate.text(currentRate)

    #calculating the maximum rate of currency of the day
    max = ->
        maxArr = []
        $.each(data.datasets, ->
            maxFromPeriod = Math.max.apply(Math, @.data).toFixed(2)
            maxArr.push(maxFromPeriod)
        )
        maxDay = Math.max.apply(Math, maxArr).toFixed(2)
        CV.maxDay.text(maxDay)

    getData = (timeout) ->
        ajax_call = ->
            $.ajax({
                url: "http://104.236.42.50/revolut/index.php",
                timeout: 3000
            }).success( (data) ->
                console.log(data)
            ).fail((jqXHR, textStatus) ->
                if(textStatus is 'timeout')
                    alert('Failed from timeout')
            )

        interval = 1000 * 60 * 15
        ajax_call()
        setInterval(ajax_call, interval);

    init = ->
        do initCanvas
        do initCalendar
        do current
        do max
        do getCurrentDate
        do getData

    return {
        init: init
    }