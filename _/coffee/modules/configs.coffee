data = {
    labels: ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"],

    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        },
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        },
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        },
        {
            label: "My First dataset",
            fillColor: "rgba(52,152,219,1)",
            highlightFill: "rgba(4,82,135,1)"
            data: [22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.5, 21.3, 22.7, 23, 23.3, 22.4, 21.7, 22.4, 23.1, 24.3, 22, 25.6, 23.5, 21.4, 23.5, 23, 22]
        }
    ]
}

data2 = {
    labels: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],

    datasets: [
        {
            label: "My Second dataset",
            fillColor: "#c0392b",
            strokeColor: "#c0392b",
            pointHighlightFill: "#c0392b",
            data: [22.7, 21.6, 22.8, 22.5, 21.3, 22.6, 21.7, 20.7, 21.3, 21.7, 22, 21.3, 22.6, 21.7, 22.4, 22.4, 21.3, 22, 21.8, 22.7, 21.9, 22.6, 22, 22, 21.6, 21, 22.9, 22.7, 22.8, 22.9]
        },
        {
            label: "My First dataset",
            fillColor: "#3498db",
            strokeColor: "#3498db",
            pointHighlightFill: "#3498db",
            data: [22.5, 21.3, 22.7, 22, 21.3, 22.4, 21.7, 20.5, 21.3, 21.7, 22, 21.3, 22.4, 21.7, 22.4, 22.1, 21.3, 22, 21.6, 22.5, 21.4, 22.5, 22, 22, 21.6, 21, 22.5, 22, 22.6, 22.6]
        }
    ]
}

options = {
# Boolean - Whether to animate the chart
    animation: true,

# Number - Number of animation steps
    animationSteps: 60,

# String - Animation easing effect
# Possible effects are:
# [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
#  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
#  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
#  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
#  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
#  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
#  easeOutElastic, easeInCubic]
    animationEasing: "easeOutQuart",

# Boolean - If we should show the scale at all
    showScale: true,

    pointDot: false,

# Boolean - If we want to override with a hard coded scale
    scaleOverride: false,

# ** Required if scaleOverride is true **
# Number - The number of steps in a hard coded scale
    scaleSteps: null,
# Number - The value jump in the hard coded scale
    scaleStepWidth: null,
# Number - The scale starting value
    scaleStartValue: null,

# String - Colour of the scale line
    scaleLineColor: "rgba(0,0,0,.1)",

# Number - Pixel width of the scale line
    scaleLineWidth: 1,

# Boolean - Whether to show labels on the scale
    scaleShowLabels: true,

# Interpolated JS string - can access value
    scaleLabel: "<%=value%>",

# Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
    scaleIntegersOnly: false,

# Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: false,

# String - Scale label font declaration for the scale label
    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

# Number - Scale label font size in pixels
    scaleFontSize: 12,

# String - Scale label font weight style
    scaleFontStyle: "normal",

# String - Scale label font colour
    scaleFontColor: "#666",

# Boolean - whether or not the chart should be responsive and resize when the browser does.
    responsive: true,

# Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,

# Boolean - Determines whether to draw tooltips on the canvas or not
    showTooltips: true,

# Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
    customTooltips: false,

# Array - Array of string names to attach tooltip events
    tooltipEvents: ["mousemove", "touchstart", "touchmove"],

# String - Tooltip background colour
    tooltipFillColor: "rgba(0,0,0,0.8)",

# String - Tooltip label font declaration for the scale label
    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

# Number - Tooltip label font size in pixels
    tooltipFontSize: 14,

# String - Tooltip font weight style
    tooltipFontStyle: "normal",

# String - Tooltip label font colour
    tooltipFontColor: "#fff",

# String - Tooltip title font declaration for the scale label
    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

# Number - Tooltip title font size in pixels
    tooltipTitleFontSize: 14,

# String - Tooltip title font weight style
    tooltipTitleFontStyle: "bold",

# String - Tooltip title font colour
    tooltipTitleFontColor: "#fff",

# Number - pixel width of padding around tooltip text
    tooltipYPadding: 6,

# Number - pixel width of padding around tooltip text
    tooltipXPadding: 6,

# Number - Size of the caret on the tooltip
    tooltipCaretSize: 8,

# Number - Pixel radius of the tooltip border
    tooltipCornerRadius: 6,

# Number - Pixel offset from point x to tooltip edge
    tooltipXOffset: 10,

# String - Template string for single tooltips
    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

# String - Template string for multiple tooltips
    multiTooltipTemplate: "<%= value %>",

# Function - Will fire on animation progression.
#    onAnimationProgress: function(){},

# Function - Will fire on animation completion.
#    onAnimationComplete: function(){}
}